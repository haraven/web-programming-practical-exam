﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace practical_exam
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Services.WebMethod]
        public static string OnQuery(string query)
        {
            string res = "{ ";
            try
            {
                using (var conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                "pwd=RESILIENCEWILLFIXIT;database=wp_exam;"))
                {
                    string[] split_query = query.Split(' ');
                    conn.Open();
                    for (int i = 0; i < split_query.Length; ++i)
                    {
                        string sql = "SELECT * FROM wp_exam.queryterms WHERE Term LIKE @myquery";
                        MySqlCommand select = new MySqlCommand(sql, conn);
                        select.Parameters.AddWithValue("@myquery", '%' + split_query[i] + '%');

                        var reader = select.ExecuteReader();
                        if (reader.HasRows)
                        {
                            res += '"' + split_query[i] + "\": \"";
                            while (reader.Read())
                            {
                                string term = reader["Term"].ToString();
                                res += term + " ";
                            }
                            res = res.Substring(0, res.Length - 1);
                            res += "\", ";
                        }

                        reader.Close();
                    }

                    if (res[res.Length - 1] == ' ' && res[res.Length - 2] == ',')
                    {
                        res = res.Substring(0, res.Length - 2);
                    }

                    res += " }";

                    return res;
                }
            }
            catch (Exception)
            {
                return "Internal error";
            }
        }

        [System.Web.Services.WebMethod]
        public static string GetDocs(string content)
        {
            string res = "{ ";
            try
            {
                using (var conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                "pwd=RESILIENCEWILLFIXIT;database=wp_exam;"))
                {
                    Dictionary<int, int> docs = new Dictionary<int, int>();
                    string[] split_query = content.Split(' ');
                    conn.Open();
                    for (int i = 0; i < split_query.Length; ++i)
                    {
                        string sql = "SELECT * FROM wp_exam.queryterms WHERE Term LIKE @myquery";
                        MySqlCommand select = new MySqlCommand(sql, conn);
                        select.Parameters.AddWithValue("@myquery", '%' + split_query[i] + '%');

                        var reader = select.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                string doclist = reader["DocumentList"].ToString();
                                string[] split_doclist = doclist.Split(' ');
                                for (int j = 0; j < split_doclist.Length; ++j)
                                {
                                    string[] split_element = split_doclist[j].Split(':');
                                    int id = Convert.ToInt32(split_element[0]);
                                    int count = Convert.ToInt32(split_element[1]);
                                    if (docs.ContainsKey(id))
                                        docs[id] += 1;
                                    else
                                        docs.Add(id, 1);
                                }
                            }
                        }

                        reader.Close();

                        foreach (KeyValuePair<int, int> entry in docs)
                        {
                            if (entry.Value.Equals(split_query.Length - 1))
                            {
                                Tuple<string, string> tmp = GetDocumentContent(entry.Key);
                                if (tmp != null)
                                    return tmp.Item1 + " | "+ tmp.Item2;
                            }
                        }

                    }

                    return "no match";
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private static Tuple<string, string> GetDocumentContent(int id)
        {
            try
            {
                using (var conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                "pwd=RESILIENCEWILLFIXIT;database=wp_exam;"))
                {
                    conn.Open();

                    string sql = "SELECT URL, Description FROM wp_exam.documents WHERE ID = @entry_id";
                    MySqlCommand select = new MySqlCommand(sql, conn);
                    select.Parameters.AddWithValue("@entry_id", id);

                    var reader = select.ExecuteReader();

                    if (reader.HasRows)
                    {
                        reader.Read();
                        Tuple<string, string> tmp = new Tuple<string, string>(reader["URL"].ToString(), reader["Description"].ToString());
                        reader.Close();

                        return tmp;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private static string TupleToJSON(Tuple<string, string> tuple)
        {
            if (tuple == null) return "";
            string res = "{ \"URL\": ";
            res += '"' + tuple.Item1 + '"';
            res += ", \"Description\": " + '"' + tuple.Item2 + '"';
            res += " }";

            return res;
        }
    }
}