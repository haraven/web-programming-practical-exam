﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="practical_exam.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>SEARCH.ME</title>
    <link rel="stylesheet" href="themes/css/ui.css" />
    <script src="script/js/jquery-2.2.3.min.js"></script>
    <script src="script/js/myscript.js"></script>
</head>
<body>
    <div class="container">
        <h1 class="welcome-msg">
            GOOGLE 2.0
        </h1>
        <form id="input-form">
            <div id="error">
            </div>
            <div id="input-group">
                <p>
                    Enter search query:
                </p>
                <br />
                <input type="text" id="search_query" />
            </div>
            <br />
            <button type="submit" id="btn-submit">SEARCH</button>
        </form>
        <div id="results">
        </div>
        <div id="queries">
            <p>Selected: </p>
            <p id="selected-queries" style="color: indianred;">
            </p>
            <button type="button" id="btn-docs">GET DOCS</button>
        </div>
        <div id="docs">
        </div>
    </div>
</body>
</html>

