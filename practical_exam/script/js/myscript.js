﻿var links;


$(document).ready(function ()
{
    $("#btn-submit").click(function ()
    {
        event.preventDefault();

        GetResults($("#search_query").val());
    });

    $("#search_query").change(function ()
    {
        GetResults($("#search_query").val());
    });

    $("#btn-docs").click(function()
    {
        $.ajax
        ({
            url: 'index.aspx/GetDocs',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify
            ({
                content: $("#selected-queries").text()
            }),
            dataType: "json",
            success: function (data) 
            {
                var res = data.d;
                var split_res = res.split(" | ");
                $("#docs").html("<p>Matches: </p><a id=\"" + split_res[0] + "\" href=\"" + split_res[0] + "\" onclick=\"GetDocContent(this)\">" + split_res[1] + "</a>");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) 
            {
                $("#error").html("<div id=\"error\">" + xmlHttpRequest.responseText + "</div>").fadeOut(4000);
            }
        });
    });
});

function GetResults(query)
{
    $.ajax
    ({
        url: 'index.aspx/OnQuery',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify
        ({
            query: query
        }),
        dataType: "json",
        success: function (data) {
            var string_list = $("#search_query").val().split(' ');
            var res = data.d;
            console.log(res);
            links = jQuery.parseJSON(res);
            $("#results").html("");
            $("#selected-queries").html("");
            $.each(links, function (i, item)
            {
                $("#results").html("<p> For \"" + i + "\" you have: </p>" + "<a style=\"cursor: pointer;\" id=\"" + item + "\" onclick=\"OnLinkClicked(this)\">" + item + "</a>" + $("#results").html());
            });
        },
        error: function (xmlHttpRequest, textStatus, errorThrown) {
            $("#error").html("<div id=\"error\">" + xmlHttpRequest.responseText + "</div>").fadeOut(4000);
        }
    });
}

function OnLinkClicked(link)
{
    $("#selected-queries").text($("#selected-queries").text() + " " + $(link).text());
}

function GetDocContent(link)
{
    console.log("NYI");
}